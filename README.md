# README #

Библиотека для мониторинга времени исполнения участков кода.

## Зависимости ##
Для запуска также требуется https://bitbucket.org/Pahtet/omr-library

## Компоненты ##
* com.omr.profiler.Profiler класс который встраивается в приложение 
* com.omr.profiler.parser.Parser консольное приложение для автоматического встраивания в проект на Java

## Другие приложения из этого проекта ##

* https://bitbucket.org/Pahtet/omr-client-agent
* https://bitbucket.org/Pahtet/omr-client-admin

## Использованные проекты ##

* https://code.google.com/p/javaparser/ Библиотека для парсинга исходных кодов