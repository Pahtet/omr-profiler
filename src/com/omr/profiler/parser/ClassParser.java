package com.omr.profiler.parser;

import japa.parser.JavaParser;
import japa.parser.ParseException;
import japa.parser.ast.CompilationUnit;
import japa.parser.ast.body.BodyDeclaration;
import japa.parser.ast.body.ConstructorDeclaration;
import japa.parser.ast.body.MethodDeclaration;
import japa.parser.ast.body.TypeDeclaration;
import japa.parser.ast.stmt.BlockStmt;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.List;

class ClassParser
{
	private static String methodName="Profiler.track();\n";
	private static String initName="Profiler.init();";
	
	/*
	 * Метод добавлющий в класс вызовы профилировщика
	 */
	public static void addToFile(File fileName) throws IOException, ParseException
	{
		FileInputStream in = new FileInputStream(fileName);
		String[] lines = getFileContent(in).split("\n");
		in.close();
		in = new FileInputStream(fileName);
		CompilationUnit cu;
		try
		{
			cu = JavaParser.parse(in);
		} finally
		{
			in.close();
		}
		List<TypeDeclaration> types = cu.getTypes();
		for (TypeDeclaration t : types)
		{
			System.out.println(t.getName() + " " + t.getBeginLine());
			List<BodyDeclaration> body = t.getMembers();
			for (BodyDeclaration b : body)
			{
				if (b instanceof ConstructorDeclaration)
				{
					ConstructorDeclaration constructor = (ConstructorDeclaration) b;
					BlockStmt methodBody = constructor.getBlock();
					System.out.println("\t" + constructor.getName() + " Constructor " + methodBody.getBeginLine() + " " + methodBody.getBeginColumn());
					lines[methodBody.getBeginLine()-1]+=initName;
				}
				if (b instanceof MethodDeclaration)
				{
					MethodDeclaration method = (MethodDeclaration) b;
					BlockStmt methodBody = method.getBody();
					System.out.println("\t" + method.getName() + " " + methodBody.getBeginLine() + " " + methodBody.getBeginColumn());
					lines[methodBody.getBeginLine()-1]+=methodName;
					lines[methodBody.getEndLine()-1]=methodName+lines[methodBody.getEndLine()-1];
				}
			}
		}
		PrintWriter writer = new PrintWriter(fileName+".new", "UTF-8");	
		for(int i=0;i<lines.length;i++)
		{
			writer.write(lines[i]);
		}
		writer.close();
	}
	
	/*
	 * Метод считывающий файл в строку
	 */
	public static String getFileContent(FileInputStream fis) throws IOException
	{
		StringBuilder sb = new StringBuilder();
		Reader r = new InputStreamReader(fis, "UTF-8"); // or whatever encoding
		char[] buf = new char[1024];
		int amt = r.read(buf);
		while (amt > 0)
		{
			sb.append(buf, 0, amt);
			amt = r.read(buf);
		}
		return sb.toString();
	}
	
}