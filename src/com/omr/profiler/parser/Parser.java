package com.omr.profiler.parser;

import japa.parser.ParseException;

import java.io.File;
import java.io.IOException;


public class Parser
{
	public static void main(String[] args) throws Exception
	{
		String targetFolder="C:/Workspace/OMR-profiler/src/com/omr/profiler/parser/test/";
		File dir;
		if(args.length == 0)
		{
			dir = new File(targetFolder);
		}
		else
		{
			dir = new File(args[0]);
		}
		
		if(!dir.isDirectory())
		{
			System.err.println("Folder '" + targetFolder + "' does not exist");
		}
		parserFolder(dir);
		
	}
	
	/*
	 * Метод добавлющий ко всем исходным файлам в папке вызовы профилировщика
	 */
	public static void parserFolder(File folder)
	{
		for(File f : folder.listFiles())
		{
			System.out.println(f.getAbsolutePath());
			if(f.isDirectory())
			{
				parserFolder(f);
			}
			if(f.isFile() && f.getName().endsWith(".java"))
			{
				try
				{
					ClassParser.addToFile(f);
				} catch (IOException | ParseException e)
				{
					e.printStackTrace();
				}
			}
		}
	}

}