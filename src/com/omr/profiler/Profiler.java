package com.omr.profiler;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class Profiler
{
	static Map<Integer, Integer> methodInvokeTime;
	static Map<String, Integer> methodNames;
	private static SenderThread sender=null;
	private static Boolean fistRun=true;
	private static Integer codeSegmentId=0;
	
	public static void init() 
	{
		synchronized (fistRun)
		{
			if(fistRun)
			{
				firstInit();
				fistRun = false;
			}
		}
	}
	
	/*
	 * Метод выполняющий первоначальную инициализацию
	 */
	private static void firstInit()
	{
		methodInvokeTime = new HashMap<Integer, Integer>();
		methodNames = new HashMap<String, Integer>();
		sender = new SenderThread();
		new Thread(sender).start();
	}
	
	/*
	 *  Метод отслеживающий время выполнение методов которые его вызывают
	 */
	public static void track()
	{
		int time = getTimeMilliseconds();
		StackTraceElement[] stack = Thread.currentThread().getStackTrace();
		String currentMethodName=stack[2].getClassName()+"."+stack[2].getMethodName();
		Integer methodId;
		synchronized (methodNames)
		{
			if(methodNames.containsKey(currentMethodName))
			{
				methodId=methodNames.get(currentMethodName);
			}
			else
			{
				synchronized (codeSegmentId)
				{
					codeSegmentId++;
					methodNames.put(currentMethodName, codeSegmentId);
					methodId=codeSegmentId;
				}				
			}
		}
		if(methodInvokeTime.containsKey(methodId))
		{
			System.out.println(currentMethodName+" time="+(time-methodInvokeTime.get(methodId))+" methodId="+methodId);
			methodInvokeTime.remove(methodId);
		}
		else
		{
			methodInvokeTime.put(methodId,time);
		}

	}
	
	/*
	 *  Возвращает текущее время в милисекундах
	 */
	private static int getTimeMilliseconds() 
	{
		return Calendar.getInstance().get(Calendar.MILLISECOND);
	}
	
	protected void finalize()throws Throwable
    {
		if(null != sender)
		{
			sender.stop();
		}
    }
}
