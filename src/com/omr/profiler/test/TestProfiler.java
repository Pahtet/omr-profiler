package com.omr.profiler.test;

import com.omr.profiler.Profiler;

public class TestProfiler
{

	public TestProfiler()
	{
		Profiler.init();
	}
	
	public void startTest()
	{
		test();
		test3();
	}
	
	private void test()
	{
		Profiler.track();
		test128();
		Profiler.track();
	}
	
	private void test128()
	{
		Profiler.track();
		try
		{
			Thread.sleep(128);
		} catch (InterruptedException e)
		{
		}
		Profiler.track();
	}
	
	private void test3()
	{
		Profiler.track();
		Profiler.track();
	}
}
