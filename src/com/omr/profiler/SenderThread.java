package com.omr.profiler;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Set;

import com.omr.data.CodeFragmentMetrics;
import com.omr.data.Command;
import com.omr.data.CommandsEnum;

class SenderThread implements Runnable
{
	private final int port=43211;
	private boolean isRun;
	private Socket connection;
	private ObjectOutputStream out;
	private ObjectInputStream in;
	private int sendedMetrics;
	private int sendedCodeFragments;
	public SenderThread()
	{
		isRun = true;
		sendedMetrics = 0;
		sendedCodeFragments = 0;
	}
	
	@Override
	public void run()
	{
		while(isRun)
		{
			try
			{
				connection = new Socket("127.0.0.1",port);
				out =  new ObjectOutputStream(connection.getOutputStream());
				in = new ObjectInputStream(connection.getInputStream());
				while(isRun)
				{
					if(Profiler.methodInvokeTime.size()>sendedMetrics)
					{
						Integer[] keys = (Integer[])Profiler.methodInvokeTime.keySet().toArray();
						for(int i = sendedCodeFragments;i<Profiler.methodInvokeTime.size();i++)
						{
							CodeFragmentMetrics cfm = new CodeFragmentMetrics();
							cfm.setCodeFragmentId(Profiler.methodInvokeTime.get(keys[i]));
							cfm.setTime(Profiler.methodInvokeTime.get(cfm.getCodeFragmentId()));
							out.writeObject(new Command(CommandsEnum.CODEMETRICS,cfm));
							out.flush();
							sendedMetrics++;
						}
					}
				}
			} catch (IOException e)
			{
				System.out.println(e.getMessage());
			}
			try
			{
				Thread.sleep(5*1024);
			} catch (InterruptedException e)
			{
				System.out.println(e.getMessage());
			}
		}
	}

	public void stop()
	{
		isRun = false;
	}

}
